//
//  main.m
//  ReedApp
//
//  Created by Reed Kemp on 5/24/15.
//  Copyright (c) 2015 Box Softwere. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
