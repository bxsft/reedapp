//
//  AppDelegate.h
//  ReedApp
//
//  Created by Reed Kemp on 5/24/15.
//  Copyright (c) 2015 Box Softwere. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>
@property (weak) IBOutlet NSTextField *myText;
- (IBAction)button1:(id)sender;
- (IBAction)button2:(id)sender;
@property (weak) IBOutlet NSButton *b1;
@property (weak) IBOutlet NSButton *b2;

@property (weak) IBOutlet NSView *window2;
@property (assign) IBOutlet NSWindow *window;

@end
