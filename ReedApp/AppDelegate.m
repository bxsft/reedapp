//
//  AppDelegate.m
//  ReedApp
//
//  Created by Reed Kemp on 5/24/15.
//  Copyright (c) 2015 Box Softwere. All rights reserved.
//


//***************
//* TOP OF FILE *
//***************

#import "AppDelegate.h"

@implementation AppDelegate
@synthesize myText = _myText;
@synthesize window = _window;
@synthesize b1 = _b1;
@synthesize b2 = _b2;
@synthesize window2 = _window2;



//********************
//* MIDDLE OF FILE   *
//********************


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    [_b2 setKeyEquivalent:@"\t"];
    [_b1 setKeyEquivalent:@"\r"];
    [_window2 setVisible:YES];
}

- (BOOL) preformKeyEquivalent:(NSEvent *)theEvent{
    if (theEvent.key == _b1.keyEquivalent){
        return YES;
    }
    
    if (theEvent.key == _b2.keyEquivalent) {
        return YES;
    }
    return NO;
        
}

- (IBAction)button1:(id)sender {
    _myText.stringValue = [NSString stringWithFormat:@"Button 1 clicked!"];
    _window.backgroundColor = [NSColor blueColor];
}

- (IBAction)button2:(id)sender {
    _myText.stringValue = [NSString stringWithFormat:@"Button 2 has been clicked!"];
    _window.backgroundColor = [NSColor greenColor];

}


//******************
//* BOTTOM OF FILE *
//******************

@end
